/*
 * Задача 2. Trade Arbitrage.
 * Необходимо написать торгового советника для поиска арбитража.
 * Арбитраж - это торговля по цепочке различных валют в надежде заработать на небольших различиях в коэффициентах. Например, есть следующие курсы валют:
 * GBP/USD: 0.67
 * RUB/GBP: 78.66
 * USD/RUB: 0.02
 * Имея 1$ и совершив цикл USD->GBP->RUB->USD, получим 1.054$. Таким образом заработав 5.4
 * Время - O(VE), Память - O(V)
 */
#if defined HOME
#include "arbitrage.h"
#endif
#include <cmath>
#include <iostream>
#include <list>
#include <set>
#include <vector>
using std::list;
using std::pair;
using std::set;
using std::vector;
struct IGraph {
  virtual ~IGraph() {}

  virtual void AddEdge(int from, int to, double cost) = 0;

  virtual int VerticesCount() const = 0;
  virtual void GetNextVertices(
      int vertex, std::vector<pair<int, double>>& vertices) const = 0;
  virtual void GetPrevVertices(
      int vertex, std::vector<pair<int, double>>& vertices) const = 0;
};
struct ArcGraph : public IGraph {
 public:
  struct Edge {
    int from, to;
    double cost;
  };

 private:
  std::vector<Edge> edges_vector;
  int vertices_count = 0;

 public:
  ArcGraph(int n) : vertices_count(n) {}
  ~ArcGraph() = default;
  ArcGraph(const IGraph* graph);

  const vector<Edge>& GetAllEdges() const;
  void AddEdge(int from, int to, double cost) override;
  void GetNextVertices(int vertex,
                       std::vector<pair<int, double>>& vertices) const override;
  void GetPrevVertices(int vertex,
                       std::vector<pair<int, double>>& vertices) const override;
  int VerticesCount() const override;
};

bool find_arbitrage(ArcGraph& graph);

#if not defined NOTMAIN
int main() {
  int VerticesCount;
  std::cin >> VerticesCount;
  ArcGraph graph(VerticesCount);
  for (int v1 = 0; v1 < VerticesCount; v1++)
    for (int v2 = 0; v2 < VerticesCount; v2++)
      if (v1 == v2)
        continue;
      else {
        double cost;
        std::cin >> cost;
        graph.AddEdge(v1, v2, -log(cost));
      }
  std::cout << (find_arbitrage(graph) ? "YES" : "NO");
  return 0;
}
#endif

bool find_arbitrage(ArcGraph& graph) {
  const double INF = 1e20;
  vector<double> distance(graph.VerticesCount());
  bool last_changed = false;
  for (int i = 0; i < graph.VerticesCount(); ++i) {
    last_changed = false;
    for (auto edge : graph.GetAllEdges())
      if (distance[edge.to] > distance[edge.from] + edge.cost) {
        distance[edge.to] = fmax(-INF, distance[edge.from] + edge.cost);
        last_changed = true;
      }
  }
  return last_changed;
}

void ArcGraph::AddEdge(int from, int to, double cost) {
  edges_vector.push_back({from, to, cost});
}

int ArcGraph::VerticesCount() const { return ArcGraph::vertices_count; }

void ArcGraph::GetNextVertices(int vertex,
                               vector<pair<int, double>>& vertices) const {
  for (auto it : ArcGraph::edges_vector) {
    if (it.from == vertex) vertices.push_back({it.to, it.cost});
  }
}

void ArcGraph::GetPrevVertices(int vertex,
                               vector<pair<int, double>>& vertices) const {
  for (auto it : ArcGraph::edges_vector)
    if (it.to == vertex) vertices.push_back({it.from, it.cost});
}

const vector<ArcGraph::Edge>& ArcGraph::GetAllEdges() const {
  return ArcGraph::edges_vector;
}

ArcGraph::ArcGraph(const IGraph* graph) : ArcGraph(graph->VerticesCount()) {
  vector<pair<int, double>> temp;
  for (int i = 0; i < vertices_count; i++) {
    temp.clear();
    graph->GetNextVertices(i, temp);
    for (auto it : temp) edges_vector.push_back({i, it.first, it.second});
  }
}
